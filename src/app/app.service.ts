import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/debounceTime';
import { User } from './User';

@Injectable({
    providedIn: 'root'
})
export class AppService {
    userInstance: User;
    externalIdenfier: string;
    private url: string = "https://api.statuspage.io/v1/pages/0ypqjmkwgqmh";
    private auth: string = "?api_key=4aeca3a4-8ee3-4d83-9f92-42f367c593e9";

    constructor(private http: HttpClient) { }

    public getGroupDetails(): Promise<any> {
        return this.http.get(this.url + "/page_access_groups.json" + this.auth).toPromise()
            .then((arrResp: any) => {
                return arrResp;
            });
    }

    public pageAccessUserRequest(reqVal: any, groups: any) {
        let userData = JSON.parse(reqVal);
        let groupId: string;
        for (let i = 0; i < groups.length; i++) {
            if (groups[i]["name"] === userData.Client) {
                groupId = groups[i]["id"];
            }
        }
        let jsonBody = {
            "page_access_user":
            {
                "page_access_group_ids": groupId,
                "email": userData.EmailAddress,
                "subscribe_to_components": true
            }
        }
        this.http.post(this.url + "/page_access_users.json" + this.auth, jsonBody).toPromise()
            .then(res => {
                console.log(res);
            });
    }

    public pageAccessGroupRequest(reqVal: any) {
        let groupData = JSON.parse(reqVal);
        let uniqueNumber = groupData.GroupName.replace(/[^0-9:,]+/, "");
        this.externalIdenfier = "CID" + uniqueNumber;
        let jsonBody = {
            "page_access_group": {
                "external_identifier": this.externalIdenfier,
                "name": groupData.GroupName
            }
        }
        this.http.post(this.url + "/page_access_groups.json" + this.auth, jsonBody).toPromise()
            .then(resp => { console.log(resp) });
    }

    public ComponentsToGroups(reqVal: any) {
        let componentsData = JSON.parse(reqVal);
        let groupID;
        this.http.get(this.url + "/page_access_groups.json" + this.auth).toPromise()
            .then((arrResp: any) => {
                let d = arrResp;
                for (let i = 0; i < d.length; i++) {
                    if (d[i]["name"] === componentsData.Client) {
                        groupID = d[i]["id"];
                    }
                }
                console.log("groupID: " + groupID);
                this.http.get(this.url + "/components.json" + this.auth).toPromise()
                    .then((arrResp: any) => {
                        let d = arrResp;
                        let compNameArray = componentsData.Components.split(",");
                        let compID = [];
                        for (let a = 0; a < compNameArray.length; a++) {
                            for (let z = 0; z < d.length; z++) {
                                if (d[z]["name"] === compNameArray[a]) {
                                    compID[a] = d[z]["id"];
                                }
                            }
                        }
                        let reqBody = {
                            "page_access_group": {
                                "name": componentsData.Client,
                                "component_ids": compID
                            }
                        }
                        this.http.patch(this.url + "/page_access_groups/" + groupID + ".json" + this.auth, reqBody).toPromise()
                            .then(resp => { console.log(resp) });
                    });
            });
    }
}
