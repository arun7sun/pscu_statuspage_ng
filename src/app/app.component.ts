import { Component, OnInit } from '@angular/core';
import * as XLSX from 'ts-xlsx';
import { AppService } from './app.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [AppService]
})
export class AppComponent {

  constructor(private appServ: AppService) { }

  arrayBuffer: any;
  bstr: any;
  arr = new Array();
  file: File;
  uploadOption: any;

  csvArray = new Array();
  obj: JSON;
  groupsDetails: any;

  ngOnInit() {
    this.getGroups();
  }

  onSelect(option) {
    this.uploadOption = option;
  }

  changeListener(event) {
    this.file = event.target.files[0];
  }

  getGroups() {
    this.appServ.getGroupDetails().then(g => {
      this.groupsDetails = g;
    }, err => err);
  }

  upload() {
    console.log(this.uploadOption)
    let fileReader = new FileReader();
    fileReader.onload = (e) => {
      this.arrayBuffer = fileReader.result;
      let data = new Uint8Array(this.arrayBuffer);
      for (let i = 0; i != data.length; ++i) {
        this.arr[i] = String.fromCharCode(data[i]);
      }
      this.bstr = this.arr.join("");
      let workbook = XLSX.read(this.bstr, { type: "binary" });
      let first_sheet_name = workbook.SheetNames[0];
      let worksheet = workbook.Sheets[first_sheet_name];
      this.csvArray = XLSX.utils.sheet_to_json(worksheet, { raw: true });
      if (this.uploadOption == "userOPT") {
        if (this.emailValidator(this.csvArray)) {
          console.log("No problem with data in uploaded file");
          for (let j = 0; j != this.csvArray.length; j++) {
            this.sleep(100);
            if (j != 0 && j % 60 == 0) {
              console.log("Waiting to pass the current server window");
              this.sleep(150000);
              console.log("Wait over")
            }
            this.appServ.pageAccessUserRequest(JSON.stringify(this.csvArray[j]), this.groupsDetails);
            console.log("Iteration :" + j);
          }
        }
        else {
          alert("No Proper data!!! Please check the console for errors,by pressing F12 key");
        }
      } else if (this.uploadOption == "grpOPT") {
        for (let j = 0; j != this.csvArray.length; j++) {
          this.appServ.pageAccessGroupRequest(JSON.stringify(this.csvArray[j]));
        }
      } else {
        for (let j = 0; j != this.csvArray.length; j++) {
          this.appServ.ComponentsToGroups(JSON.stringify(this.csvArray[j]));
        }
      }
    }
    fileReader.readAsArrayBuffer(this.file);
  }

  emailValidator(objArr) {
    let emailRE = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/);
    let sortedOA = objArr.slice().sort();
    for (let num = 0; num != objArr.length; num++) {
      try {
        if (objArr[num].EmailAddress == undefined || (!emailRE.test(objArr[num].EmailAddress))) {
          throw new Error("Invalid Email")
        }
      }
      catch (ex) {
        console.error(ex + ":" + objArr[num].EmailAddress + " in row number ==> " + (num + 1))
        return false
      }
    }
    let dupMails = [];
    for (let i = 0; i < sortedOA.length - 1; i++) {
      if (sortedOA[i + 1].EmailAddress == sortedOA[i].EmailAddress) {
        dupMails.push(sortedOA[i]);
      }
    }
    if (dupMails === undefined || dupMails.length === 0) {
      console.log("There is no duplication")
    } else {
      console.log("There are duplications in data as follows: ")
      for (let er = 0; er != dupMails.length; er++) {
        console.log(dupMails[er].EmailAddress + " in excelsheet row number:" + dupMails[er].__rowNum__);
      }
      return false
    }
    return true
  }
  sleep(milliseconds) {
    let start = new Date().getTime();
    for (let i = 0; i < 1e9; i++) {
      if ((new Date().getTime() - start) > milliseconds) {
        break;
      }
    }
  }
}
